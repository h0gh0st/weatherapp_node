// Packages
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');

// Routes
const index = require('./routes/index');

// Init
const app = express();
  // view engine setup
  // app.set('views', path.join(__dirname, 'views'));
  // app.set('view engine', 'ejs');
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));

  // views
  // app.use('/', index);
  app.use('/', express.static(__dirname + '/views'));
  app.use('/public', express.static(__dirname + '/public'));
  app.use('/bower', express.static(__dirname + '/bower_components'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

module.exports = app;
