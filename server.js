const http = require('http');
const normalizePort = require('normalize-port');
const app = require('./app');

const port = normalizePort(process.env.PORT || '2020');
const server = http.createServer(app).listen(port, () => {
  console.log(`listening on localhost:${port}`);
});
